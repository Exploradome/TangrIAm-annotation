from unittest import TestCase
import numpy as np
import cv2

from src.form_classification.model import Model

class TestModel(TestCase):

    @classmethod
    def setUpClass(self):
        # super().__init__(methodName)
        print("Loading model...")
        self.model = Model('models/model_13classes_20231204-215048_synth_100_128_15', False, 3, 224, 224)
        self.model.get_trained_model()
        print("Done Loading model")

    def test_return_top_3_values_with_none(self):
        # Given
        predictions = np.array([0.6, 0.15, 0.10, 0.65, 0.4])

        # When
        top_3_predictions = self.model.get_top_n_results(predictions, n=3)

        # Then
        assert top_3_predictions == {2: predictions[3], 3: predictions[4], 0: predictions[1]}

    def test_return_none_values(self):
        # Given
        predictions = np.array([0.7, 0.15, 0.10, 0.65, 0.4])

        # When
        top_3_predictions = self.model.get_top_n_results(predictions, n=3)

        # Then
        assert top_3_predictions == None

    def test_get_top_n_predictions_renard(self):
        # Given
        top_n = 3
        img = cv2.imread('tests/test_data/renard/renard01_toclassify.png')

        # When
        top_predictions, predictions = self.model.get_top_n_predictions(img, top_n)
        
        # Then
        print(top_predictions)
        assert list(top_predictions.keys()).index(10) == 0
        assert top_predictions == {10: top_predictions[10], 7: top_predictions[7], 2: top_predictions[2]}

    def test_get_top_n_predictions_various(self):
        label_imgs = [
                        ('bateau/bateau01_toclassify.png', 1),
                        ('bateau/bateau02_toclassify.png', 1),
                        ('bateau/bateau03_toclassify.png', 1),
                        ('bateau/bateau04_toclassify.png', 1),
                        ('bateau/bateau05_toclassify.png', 1),
                        ('bateau/bateau06_toclassify.png', 1),
                        ('bateau/bateau07_toclassify.png', 1),
                        ('bateau/bateau08_toclassify.png', 1),
                        ('bateau/bateau09_toclassify.png', 1),
                        ('chat/chat_01_toclassify.png', 3),
                        ]
        for img, label in list(label_imgs):
            self.assert_get_top_n_predictions_per_label(img, label)

    def test_get_top_n_predictions_none(self):
        label_imgs = [
                        ('none/hand chat (3).png', 0),
                        ('none/hand coeur (6).png',  0),
                        ('none/hand maison (1).png',  0),
                        ('none/hand maison (2) reversed.png', 0),
                        ('none/none hand (41).png', 0),
                        ('none/none hand (42).png', 0),
                        ('none/none large (2).png', 0),
                        ("none/none_real (1)_toclassify.png",  0),
                        ("none/none_real (2)_toclassify.png",  0),
                        ("none/none_real (3)_toclassify.png",  0),
                        ("none/none_real (4)_toclassify.png",  0),
                        ("none/scattered_real (5)_toclassify.png",  0),
                        ("none/none_real (6)_toclassify.png",  0),
                        ("none/scattered_real (7)_toclassify.png",  0),
                        ("none/none_real (8)_toclassify.png",  0),
                        ("none/none_real (9)_toclassify.png",  0),
                        ("none/none_real (10)_toclassify.png", 0),
                        ("none/none_real (11)_toclassify.png", 0)
                        ]
        for img, label in list(label_imgs):
            self.assert_get_top_n_predictions_per_label(img, label)
            
    def test_get_top_n_predictions_for_synthetic(self):
        label_imgs = [
                        ("synthetic/none_synthetic_0.png", 0, 0.7),
                        ("synthetic/bateau_synthetic_0.png", 1, 0.7),
                        ("synthetic/bol_synthetic_0.png", 2, 0.7),
                        ("synthetic/chat_synthetic_0.png", 3, 0.7),
                        ("synthetic/coeur_synthetic_0.png", 4, 0.7),
                        ("synthetic/cygne_synthetic_0.png", 5, 0.7),
                        ("synthetic/lapin_synthetic_0.png", 6, 0.7),
                        ("synthetic/maison_synthetic_0.png", 7, 0.7),
                        ("synthetic/marteau_synthetic_0.png", 8, 0.7),
                        ("synthetic/montagne_synthetic_0.png", 9, 0.7),
                        ("synthetic/pont_synthetic_0.png", 10, 0.7),
                        ("synthetic/renard_synthetic_0.png", 11, 0.7),
                        ("synthetic/tortue_synthetic_0.png", 12, 0.7),
                       ]
        for img, label, score in list(label_imgs):
            self.assert_get_top_n_predictions_per_label(img, label, score)
 
    def assert_get_top_n_predictions_per_label(self, img, expected_label, expected_score = 0.5):
            frame = cv2.imread(f'tests/test_data/{img}')

            # When
            top_predictions, predictions = self.model.get_top_n_predictions(frame)
            print('Evaluating', img, '=>', top_predictions, '\n')

            if expected_label == 0:
                assert top_predictions is None and predictions[0] > expected_score
            else:
                expected_label -= 1
                top_predictions = dict(top_predictions)
                assert top_predictions is not None
                assert expected_label in list(top_predictions.keys())
                assert list(top_predictions.keys()).index(expected_label) == 0 
                assert top_predictions[expected_label] > expected_score
            
    def test_process_prediction_or_reason(self):
        img_label_reasons = [
                        ("bateau/bateau_camera_raw.png", 1, 'ok'),
                        ("maison/maison_camera_raw.png", 7, 'ok'),
                        ("none/hand_01_raw.png", 0, "too_big"),
                        ("none/hand_02_raw.png", 0, "too_big"),
                        ("none/hand_03_raw.png", 0, "too_big"),
                        ("none/scattered_01_raw.png", 0, "too_big"),
                        ("none/scattered_02_raw.png", 0, "too_big"),
                        ("none/scattered_04_raw.png", 0, "too_big")
                       ]
        for img, label, reason in list(img_label_reasons):
            self.assert_process_prediction_or_reason(img, label, reason)
                      
    def assert_process_prediction_or_reason(self, img, expected_label, expected_reason, expected_score = 0.5):
        frame = cv2.imread(f'tests/test_data/{img}')
        rotation = cv2.ROTATE_90_CLOCKWISE
        cropped_frame = frame[280:780, 1300:1750]
                                
        result = self.model.process_frame_and_predict(cropped_frame, rotation, True)
        image_with_bounding_box, segmented_and_cropped_image, \
        image_to_classify, cache_explanation, top_predictions, reason = result
        cv2.imwrite(f'tests/test_data/{img}_bb_cropped.png', segmented_and_cropped_image)
        if expected_label == 0:
            print('Evaluating', img, 'as', expected_label, top_predictions, reason, "==", expected_reason, '\n')
            assert top_predictions is None and reason.index(expected_reason) == 0
        else:
            expected_label -= 1
            print('Evaluating', img, 'as', expected_label, top_predictions, reason, "==", expected_reason, '\n')
            assert top_predictions is not None
            assert expected_label in dict(top_predictions).keys()
            assert list(dict(top_predictions).keys()).index(expected_label) == 0 
            assert dict(top_predictions)[expected_label] > expected_score