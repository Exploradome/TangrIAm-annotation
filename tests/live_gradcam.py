import sys
import cv2
import numpy as np

from src.form_classification.model import Model
from src.form_classification.gradcam import GradCam

model = Model("models/model_13classes_20231204-215048_synth_100_128_15", False, 3, 224, 224)
model.get_trained_model()

mock = True
if not mock:
    cap = cv2.VideoCapture(0+cv2.CAP_DSHOW)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
else:
    cap = cv2.VideoCapture()
focus = 10
brightness = 35
contrast = 25
exposure = -5
print("Press the following key (lowercase or caps-lock to change the setting:")
print("0,1,2: Switch to another webcam")
print("mock: use mock camera")
print("c/C  : Constrast")
print("b/B  : Brightness")
print("f/F  : Focus")
print("e/E  : Exposure")
print("S    : Open DirectShow settings")

while True:
    try:
        if mock:
            frame = cv2.imread('tests/test_data/bateau/bateau07_raw.png')
            crop_dimensions = (0, 500, 0, 500)
        else:
            ret, frame = cap.read()
            crop_dimensions = (0, 460, 0, 300)
        cropped_frame = frame[crop_dimensions[0]:crop_dimensions[1], crop_dimensions[2]:crop_dimensions[3]]
        cv2.imshow('org', cropped_frame)
        rotation = cv2.ROTATE_90_CLOCKWISE        
        image_with_bounding_box, segmented_and_cropped_image, \
        image_to_classify, explanation, top_predictions, reason \
            = model.process_frame_and_predict(cropped_frame, rotation, True)
        
        if explanation is not None:
            cv2.imshow('heatmap', explanation)
        if image_to_classify is not None:
            cv2.imshow('image_to_classify', image_to_classify)
        if segmented_and_cropped_image is not None:
            cv2.imshow('segmented_and_cropped_image', segmented_and_cropped_image)
        if image_with_bounding_box is not None:
            cv2.imshow('image_with_bounding_box', image_with_bounding_box)

        print(top_predictions, reason)
    except OSError:
        print()

    key = cv2.waitKey(10) & 0xFF
    if key == ord('m'):
        print("Switch to Mock camera")
        mock = not mock
        if mock:
            cap.release()
            print("Switch to Mock camera")
        else:
            print("Switch to First camera")
            cap = cv2.VideoCapture(0+cv2.CAP_DSHOW)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    if key == ord('0'):
        print("Switch to First camera")
        mock = False
        cap.release()
        cap = cv2.VideoCapture(0+cv2.CAP_DSHOW)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    if key == ord('1'):
        mock = False
        print("Switch to 2nd camera")
        cap.release()
        cap = cv2.VideoCapture(1+cv2.CAP_DSHOW)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    if key == ord('2'):
        mock = False
        print("Switch to 3rd camera")
        cap.release()
        cap = cv2.VideoCapture(2+cv2.CAP_DSHOW)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

    if key == ord('S'):
        print("Open DirectShow settings")
        cap.set(cv2.CAP_PROP_SETTINGS, 1)

    if key == ord('F'):
        focus = focus + 5
        print("FOCUS =", focus)
        cap.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        cap.set(cv2.CAP_PROP_FOCUS, focus)
    if key == ord('f'):
        focus = focus - 5
        print("FOCUS =", focus)
        cap.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        cap.set(cv2.CAP_PROP_FOCUS, focus)

    if key == ord('B'):
        brightness = brightness + 10
        print("brightness =", brightness)
        cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
    if key == ord('b'):
        brightness = brightness - 10
        print("brightness =", brightness)
        cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
        
    if key == ord('C'):
        contrast = contrast + 10
        print("contrast =", contrast)
        cap.set(cv2.CAP_PROP_CONTRAST, contrast)
    if key == ord('c'):
        contrast = contrast - 10
        print("contrast =", contrast)
        cap.set(cv2.CAP_PROP_CONTRAST, contrast)
        
    if key == ord('E'):
        exposure = exposure + 0.5
        print("exposure =", exposure)
        cap.set(cv2.CAP_PROP_EXPOSURE, exposure)
    if key == ord('e'):
        exposure = exposure - 0.5
        print("exposure =", exposure)
        cap.set(cv2.CAP_PROP_EXPOSURE, exposure)

    if key == ord('q'):
        break
# When everything done, release the capture
if cap is not None:
    cap.release()
cv2.destroyAllWindows()