from unittest import TestCase

import cv2
import os
import numpy as np

from src.form_classification.image_processor import ImageProcessor


def compare_two_images(imageA, imageB):
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype('float') - imageB.astype('float')) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err

class TestImageProcessor(TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.data_path = os.path.join(os.path.dirname(__file__), 'test_data')

    def test_segment_grayscale_boat_image(self):
        # Given
        image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_raw.png'), cv2.IMREAD_GRAYSCALE)
        expected_segmented_image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_segmented.png'), cv2.IMREAD_GRAYSCALE)

        # When
        segmented_img = ImageProcessor.segment_image(image)

        # Then
        for row in range(segmented_img.shape[0]):
            assert all([(element == 0 or element == 255) for element in segmented_img[row]])

        assert compare_two_images(segmented_img, expected_segmented_image) < 0.1

    def test_mindistance_between_contours(self):
        contours = [np.array([[0, 0], [0, 2], [1, 2], [1, 0]]),
                    np.array([[5, 5], [5, 6], [6, 6], [6, 5]]),
                    np.array([[10, 10], [10, 11], [11, 11], [11, 10]]),
                    np.array([[15, 15], [15, 17], [18, 17], [18, 15]])]
        _, bboxes, _ = ImageProcessor.draw_bounding_boxes(contours, None, None, 0, 1000000)
        mean = ImageProcessor.mean_between_bboxes(bboxes)
        print('test_mindistance_between_contours', mean)
        assert 3.13 < ImageProcessor.mean_between_bboxes(bboxes) < 3.14

    def test_mindistance_between_touching_contours(self):
        contours = [np.array([[0, 0], [0, 10], [5, 10], [5, 0]]),
                    np.array([[5, 5], [5, 20], [20, 20], [20, 5]]),
                    np.array([[20, 20], [25, 20], [25, 30], [20, 30]]),
                    np.array([[15, 15], [15, 17], [18, 17], [18, 15]])]
        _, bboxes, _ = ImageProcessor.draw_bounding_boxes(contours, None, None, 0, 1000000)
        mean = ImageProcessor.mean_between_bboxes(bboxes)
        print('test_mindistance_between_touching_contours', mean)
        assert ImageProcessor.mean_between_bboxes(bboxes) == 0

    def test_return_contours_for_boat_image(self):
        # Given
        given_segmented_image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_segmented.png'), cv2.IMREAD_GRAYSCALE)

        # When
        contours = ImageProcessor.find_contours(given_segmented_image)

        # Then
        assert len(contours) == 27

    def test_draw_bounding_box_on_grey_boat_image(self):
        # Given
        expected_outer = (0, 7, 423, 843)

        given_gray_image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_raw.png'), cv2.IMREAD_GRAYSCALE)
        given_gray_image_outer = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_raw.png'), cv2.IMREAD_GRAYSCALE)
        given_color_image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_raw.png'), cv2.IMREAD_COLOR)
        given_segmented_image = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_segmented.png'), cv2.IMREAD_GRAYSCALE)

        expected_bounding_box = cv2.imread(os.path.join(self.data_path, 'bateau/bateau01_bb_outer.png'), cv2.IMREAD_GRAYSCALE)

        # When
        contours = ImageProcessor.find_contours(given_segmented_image)
        contours, bboxes, reason = ImageProcessor.draw_bounding_boxes(contours,
                                                                given_gray_image,
                                                                given_color_image,
                                                                2000, 300000)
        outer = ImageProcessor.get_outer_box(bboxes)
        x, y, w, h = outer
        ImageProcessor.draw_rectangle(given_gray_image_outer, x, y, w, h)
        # Then
        #cv2.imwrite("bateau01_bb_outer.png", given_gray_image_outer)
        #cv2.imshow("given_gray_image_outer", given_gray_image_outer)
        #cv2.imshow("expected_bounding_box", expected_bounding_box)
        assert compare_two_images(given_gray_image_outer, expected_bounding_box) < 0.15
        assert (x, y, w, h) == expected_outer

    def generic_test_crop_image(self, name, is_none=False, save_expected=False, expected_reason = 'ok'):
        # Given
        given_raw_image = cv2.imread(os.path.join(self.data_path, name + '_raw.png'))

        expected_image_bb_color_path = os.path.join(self.data_path, name + '_bb_color.png')
        expected_image_bb_crop_path = os.path.join(self.data_path, name + '_bb_crop.png')
        expected_image_to_classify_path = os.path.join(self.data_path, name + '_toclassify.png')
        expected_image_bb_color = cv2.imread(expected_image_bb_color_path)
        expected_image_bb_crop = cv2.imread(expected_image_bb_crop_path)
        expected_image_to_classify = cv2.imread(expected_image_to_classify_path)

        # When
        image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason = \
            ImageProcessor.process_image(given_raw_image, 224, 224, cv2.ROTATE_90_CLOCKWISE)
        print('Evaluating generic_test_crop_image', name, '=>', is_none, reason, '==', expected_reason, '\n')
        if save_expected:
            cv2.imwrite(expected_image_bb_color_path, image_with_bounding_box)
            cv2.imwrite(expected_image_bb_crop_path, segmented_and_cropped_image)
            if image_to_classify is not None:
                cv2.imwrite(expected_image_to_classify_path, image_to_classify)
        # Then
        else:
            assert compare_two_images(expected_image_bb_color, image_with_bounding_box) < 0.15
            assert compare_two_images(expected_image_bb_crop, segmented_and_cropped_image) < 0.15
            assert reason.index(expected_reason) == 0
            if not is_none:
                assert image_to_classify is not None and compare_two_images(expected_image_to_classify, image_to_classify) < 0.15

    def test_crop_bateau7_image(self):
        self.generic_test_crop_image("bateau/bateau07")

    def test_crop_bateau8_image(self):
        self.generic_test_crop_image("bateau/bateau08")
    
    def test_crop_bateau9_real_image(self):
        self.generic_test_crop_image("bateau/bateau09")
    
    def test_crop_none_real_image(self):
        self.generic_test_crop_image("none/none_real (1)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (2)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (3)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (4)", True, False, "too_big")
        self.generic_test_crop_image("none/scattered_real (5)", True, False, "scattered")
        self.generic_test_crop_image("none/none_real (6)", True, False, "too_big")
        self.generic_test_crop_image("none/scattered_real (7)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (8)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (9)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (10)", True, False, "too_big")
        self.generic_test_crop_image("none/none_real (11)", True, False, "too_big")
