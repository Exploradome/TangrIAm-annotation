import sys
import cv2

# for each file in the folder
#   read the image
#   convert the image to black and white
#   save the image as png
import os
from os import listdir
from os.path import isfile, join
from PIL import Image

# get command line parameter
mypath = sys.argv[1]
files = [f for f in listdir(mypath) if isfile(join(mypath, f)) and f.lower().endswith('.jpg')]
for file in files:
    print(file)
    img = cv2.imread(join(mypath, file))
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, gray = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY)
    cv2.imshow('image', gray)
    new_file = os.path.splitext(file)[0] + '.png'
    print("save as", new_file)
    cv2.imwrite(join(mypath, new_file), gray)

