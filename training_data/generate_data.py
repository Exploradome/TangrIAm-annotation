import random
import PIL
import cv2
import numpy as np
from src.form_classification.image_processor import ImageProcessor
from PIL import Image

nb_synth = 100
forecolor = (0, 0, 0)
bgcolor = (255, 255, 255)
unit = 46
border = 3
hypothenuse = 1.41421356237
pieces = {
    'small_triangle':   [[0, 0], [unit,  unit], [0, unit]],
    'medium_triangle':  [[0, 0], [hypothenuse * unit, hypothenuse * unit], [0, hypothenuse * unit]],
    'large_triangle':   [[0, 0], [2 * unit, 2 * unit], [0, 2 * unit]],
    'square':           [[0, 0], [unit, 0], [unit, unit], [0, unit]], 
    'parallelogram':    [[0, 0], [unit,  unit], [unit, 2 * unit], [0, unit]],
    'parallelogramH':   [[0, 0], [unit, 0], 
                         [2 * unit, unit], 
                         [unit, unit]],
}

shapes = {
    'bateau': [
        [pieces['large_triangle'], 2, 3, 225],
        [pieces['large_triangle'], 2+2*hypothenuse, 3, 135],
        [pieces['small_triangle'], 0, 3, -90],
        [pieces['small_triangle'], 1, 2, -90],
        [pieces['square'], 1, 2, 0],
        [pieces['medium_triangle'], 1, 3, -45],
        [pieces['parallelogram'], 2, 4, -90],
    ],
    'bol': [
        [pieces['large_triangle'], 0, 0, -45],
        [pieces['large_triangle'], 2*hypothenuse, 0, -45],
        [pieces['small_triangle'], 2.5*hypothenuse, 0.5*hypothenuse, 90+45],
        [pieces['small_triangle'], 2*hypothenuse, hypothenuse, -45],
        [pieces['square'], 1.5*hypothenuse, 0.5*hypothenuse, 45],
        [pieces['medium_triangle'], 1+2*hypothenuse, 1+hypothenuse, 90+45],
        [pieces['parallelogram'], 1.5*hypothenuse, 0.5*hypothenuse, -45],
    ],
    'chat': [
        [pieces['large_triangle'], 1.5*hypothenuse, 4.5, -90],
        [pieces['large_triangle'], 2+.5*hypothenuse, 3.5*hypothenuse-1, -135],
        [pieces['small_triangle'], 0, hypothenuse, -135],
        [pieces['small_triangle'], hypothenuse, 0, 45],
        [pieces['square'], 0.5*hypothenuse, 0.5*hypothenuse, 45],
        [pieces['medium_triangle'], 2+0.5*hypothenuse, 1.5*hypothenuse-1, 45],
        [pieces['parallelogram'], 0.5*hypothenuse, 1.5*hypothenuse, -90],
    ],
    'coeur': [
        [pieces['large_triangle'], 2*hypothenuse, hypothenuse,  135],
        [pieces['large_triangle'], hypothenuse, hypothenuse, -45],
        [pieces['small_triangle'], 2.5*hypothenuse, 0.5*hypothenuse, 135],
        [pieces['small_triangle'], hypothenuse, 2*hypothenuse, -135],
        [pieces['square'], 1.5*hypothenuse, 1.5*hypothenuse, 45],
        [pieces['medium_triangle'], hypothenuse, 2*hypothenuse, 180],
        [pieces['parallelogram'], 1.5*hypothenuse, 0.5*hypothenuse, -45],
    ],
    'cygne': [
        [pieces['large_triangle'], 1.5*hypothenuse+1, hypothenuse-1, 45],
        [pieces['large_triangle'], 1.5*hypothenuse+1, 3*hypothenuse-1, -135],
        [pieces['small_triangle'], 0, 1.5*hypothenuse, -135],
        [pieces['small_triangle'], 2*hypothenuse+1, 1.5*hypothenuse-1, -45],
        [pieces['square'], 0.5*hypothenuse, 0, 45],
        [pieces['medium_triangle'], 0.5*hypothenuse + 2, 2*hypothenuse, 135],
        [pieces['parallelogram'], hypothenuse, 0.5*hypothenuse, 45],
    ],
    'lapin': [
        [pieces['large_triangle'], 0.5*hypothenuse, 2*hypothenuse, 0],
        [pieces['large_triangle'], 1.5*hypothenuse, hypothenuse, 45],
        [pieces['small_triangle'], 0.5*hypothenuse, hypothenuse+2, 45],
        [pieces['small_triangle'], 1.5*hypothenuse, 2*hypothenuse, 0],
        [pieces['square'], 1.5*hypothenuse, hypothenuse, 0],
        [pieces['medium_triangle'], 1.5*hypothenuse, hypothenuse, 180],
        [pieces['parallelogram'], 0, 0.5*hypothenuse, -45],
    ],
    'maison': [
        [pieces['large_triangle'], 1+2*hypothenuse, 2, 135],
        [pieces['large_triangle'], 0.5+2*hypothenuse, 2+hypothenuse, 135],
        [pieces['small_triangle'], 0.5+hypothenuse, 2, -45],
        [pieces['small_triangle'], 0.5+2*hypothenuse, 2, 45],
        [pieces['square'], 1, 0, 0],
        [pieces['medium_triangle'], 0.5+hypothenuse, 2, 90],
        [pieces['parallelogram'], 0, 2, -90],
    ],
    'marteau': [
        [pieces['large_triangle'], 2*hypothenuse, hypothenuse, 135],
        [pieces['large_triangle'], hypothenuse, 0, -45],
        [pieces['small_triangle'], 2*hypothenuse, hypothenuse, 90],
        [pieces['small_triangle'], 2*hypothenuse - 1, hypothenuse+2, -90],
        [pieces['square'], 2*hypothenuse-1, hypothenuse+2, 0],
        [pieces['medium_triangle'], 2*hypothenuse, hypothenuse, -90],
        [pieces['parallelogramH'], 2*hypothenuse, hypothenuse, 90],
    ],
    'montagne': [
        [pieces['large_triangle'], 2*hypothenuse, 1+hypothenuse, 135],
        [pieces['large_triangle'], 4*hypothenuse+1, 1+hypothenuse, 135],
        [pieces['small_triangle'], 2*hypothenuse+0.5, hypothenuse, 135],
        [pieces['small_triangle'], 3*hypothenuse+0.5, hypothenuse, 135],
        [pieces['square'], 2*hypothenuse+0.5, 0, 45],
        [pieces['medium_triangle'], 2*hypothenuse-1, hypothenuse, -45],
        [pieces['parallelogram'], 2*hypothenuse, 1+hypothenuse, -90],
    ],
    'pont': [
        [pieces['large_triangle'], 0, 2, -90],
        [pieces['large_triangle'], 5, 0, 0],
        [pieces['small_triangle'], 3, 0, 90],
        [pieces['small_triangle'], 5, 1, 180],
        [pieces['square'], 3, 1, 0],
        [pieces['medium_triangle'], 5, 1, 135],
        [pieces['parallelogram'], 2, 1, -90],
    ],
    'renard': [
        [pieces['large_triangle'], 5, 0, 0],
        [pieces['large_triangle'], 5, 2+hypothenuse, 180],
        [pieces['small_triangle'], 3, 1+hypothenuse, 90],
        [pieces['small_triangle'], 5+hypothenuse, 0, 45],
        [pieces['square'], 0, 2+hypothenuse, 0],
        [pieces['medium_triangle'], 4, 1+hypothenuse, 135],
        [pieces['parallelogram'], 0, 2+hypothenuse, -90],
    ],
    'tortue': [
        [pieces['large_triangle'], 3, 0, 90],
        [pieces['large_triangle'], 1, 2, -90],
        [pieces['small_triangle'], 1-0.5*hypothenuse, 2+0.5*hypothenuse, -135],
        [pieces['small_triangle'], 3+0.5*hypothenuse, 2-0.5*hypothenuse, 45],
        [pieces['square'], 4, 0, 0],
        [pieces['medium_triangle'], 1, 0, 45],
        [pieces['parallelogramH'], 3, 2, -90],
    ],
}
def rotate_image(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result

def rotate_piece(piece, angle):
    rotated_piece = np.zeros([len(piece), 2])
    for i in range(len(piece)):
        x = piece[i][0]
        y = piece[i][1]
        rotated_piece[i] = [x * np.cos(np.deg2rad(angle)) - y * np.sin(np.deg2rad(angle)), x * np.sin(np.deg2rad(angle)) + y * np.cos(np.deg2rad(angle))]
    return rotated_piece

def draw_bg():
    bg = cv2.imread("training_data/bg.png")
    bg = cv2.resize(bg, (28 * unit, 28 * unit))
    # place bg randomly in image (rotation and offset)
    bg = rotate_image(bg, random.randint(-15, 15))
    bg_crop_offset = [random.randint(3, 8) * unit, random.randint(3, 8) * unit]
    bg = bg[bg_crop_offset[0]:14 * unit + bg_crop_offset[0], bg_crop_offset[1]:14 * unit + bg_crop_offset[1]]
    return bg

def draw_shape(shape_name):
    shape = shapes[shape_name]
    image = np.zeros([14 * unit, 14 * unit, 3], dtype=np.uint8)

    image = cv2.add(image, draw_bg())
    for piece in list(shape):
        piece_shape = piece[0]
        piece_x = piece[1] * unit
        piece_y = piece[2] * unit
        piece_angle = piece[3]

        piece_shape = rotate_piece(piece_shape, piece_angle)
        piece_shape = rotate_piece(piece_shape, random.randint(-5*border, 5*border)/10.)
        piece_shape = piece_shape + np.array([piece_x, piece_y]) + np.array([2*unit, 2*unit])
        piece_shape = piece_shape + np.array([random.randint(-3.*border, 3.*border)/3., random.randint(-3.*border, 3.*border)/3.])
        cv2.fillPoly(image, pts = [np.array(piece_shape,  dtype=np.int32)], color = forecolor)
        cv2.polylines(image, pts = [np.array(piece_shape,  dtype=np.int32)], color = bgcolor, thickness = 1, isClosed = True)
    return image

def draw_transparent_image(background, foreground, x_offset=None, y_offset=None):
    bg_h, bg_w, bg_channels = background.shape
    fg_h, fg_w, fg_channels = foreground.shape

    assert bg_channels == 3, f'background image should have exactly 3 channels (RGB). found:{bg_channels}'
    assert fg_channels == 4, f'foreground image should have exactly 4 channels (RGBA). found:{fg_channels}'

    # center by default
    if x_offset is None: x_offset = (bg_w - fg_w) // 2
    if y_offset is None: y_offset = (bg_h - fg_h) // 2

    w = min(fg_w, bg_w, fg_w + x_offset, bg_w - x_offset)
    h = min(fg_h, bg_h, fg_h + y_offset, bg_h - y_offset)

    if w < 1 or h < 1: return

    # clip foreground and background images to the overlapping regions
    bg_x = max(0, x_offset)
    bg_y = max(0, y_offset)
    fg_x = max(0, x_offset * -1)
    fg_y = max(0, y_offset * -1)
    foreground = foreground[fg_y:fg_y + h, fg_x:fg_x + w]
    background_subsection = background[bg_y:bg_y + h, bg_x:bg_x + w]

    # separate alpha and color channels from the foreground image
    foreground_colors = foreground[:, :, :3]
    alpha_channel = foreground[:, :, 3] / 255  # 0-255 => 0.0-1.0

    # construct an alpha_mask that matches the image shape
    alpha_mask = np.dstack((alpha_channel, alpha_channel, alpha_channel))

    # combine the background with the overlay image weighted by alpha
    composite = background_subsection * (1 - alpha_mask) + foreground_colors * alpha_mask

    # overwrite the section of the background image that has been updated
    background[bg_y:bg_y + h, bg_x:bg_x + w] = composite

def draw_none():
    hands = [   'none_hand (10).png',  
                'none_hand (20).png',
                'none_hand (41).png',
                'none_hand (42).png',
                'none_hand (40).png',
                'none_hand (50).png',
                'none_hand (60).png',
                ]
    for i in range(nb_synth):
        image = np.zeros([8 * unit, 8 * unit, 3], dtype=np.uint8)

        label = list(shapes.keys())[random.randint(0, len(shapes.keys())-1)]
        shape = draw_shape(label)
        shape = rotate_image(shape, random.randint(-5, 5))
        shape_crop_offset = [int(random.randint(10, 15)/10. * unit), int(random.randint(10, 15)/10. * unit)]
        shape = shape[shape_crop_offset[0]:8 * unit + shape_crop_offset[0], shape_crop_offset[1]:8 * unit + shape_crop_offset[1]]
        image = cv2.add(image, shape)

        hand = hands[random.randint(0, len(hands)-1)]
        hand = cv2.imread(f"training_data/{hand}", cv2.IMREAD_UNCHANGED)
        hand = cv2.resize(hand, (5 * unit, 5 * unit))
        hand = rotate_image(hand, random.randint(-15,15))
        hand_crop_offset = [random.randint(0 * unit, 4 * unit), random.randint(unit, 3 * unit)]
        draw_transparent_image(image, hand, hand_crop_offset[0], hand_crop_offset[1])
        image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason = ImageProcessor.process_image(image, 224, 224, None, 0, 400) 
        cv2.imshow("image_with_bounding_box", image_to_classify)
        cv2.imwrite(f'training_data/_none_/none_synthetic_{i}.png', image_to_classify)
        cv2.waitKey(1)

def draw_all_shapes():
    for label in shapes.keys():
        print("Generating image for", label)
        for i in range(nb_synth):
            shape = draw_shape(label)
            # cv2.imshow(label, shape)
            image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason = ImageProcessor.process_image(shape, 224, 224, None) 
            if image_to_classify is not None:
                cv2.imshow('image_to_classify', image_to_classify)
                # cv2.imshow(label+'image_with_bounding_box', image_with_bounding_box)
                # cv2.imshow(label+'segmented_and_cropped_image', segmented_and_cropped_image)
                cv2.imwrite(f'training_data/{label}/{label}_synthetic_{i}.png', image_to_classify)
            else:
                cv2.imshow(label+' is not identified', image_with_bounding_box)
                print("no image for", label)
            cv2.waitKey(1)

cv2.waitKey() 
