import configparser
import cv2

class CameraHandler:

    def __init__(self, config: configparser.ConfigParser):
        self.vid = None
        self.camera_id = int(config['ID'])
        self.dshow = config.getboolean('DSHOW')
        self.fps = int(config['FPS'])
        self.width = int(config['WIDTH'])
        self.height = int(config['HEIGHT'])
        self.exposure = float(config['EXPOSURE'])
        self.contrast = int(config['CONTRAST'])
        self.brightness = int(config['BRIGHTNESS'])
        self.focus = int(config['FOCUS'])

    def destroy(self):
        if self.vid is not None and self.vid.isOpened():
            print("Turn off camera")
            cv2.destroyAllWindows()
            self.vid.release()

    def __del__(self):
        self.destroy()

    def set_camera_id(self, camera_id):
        if self.vid is not None:
            self.vid.release()
        self.camera_id = camera_id
        self.open_camera()

    def open_camera(self):
        if self.camera_id == -1:
            print('Skipping camera')
            return
        print(f'Opening camera {self.camera_id}, dshow:', self.dshow)
        self.vid = cv2.VideoCapture(self.camera_id + (cv2.CAP_DSHOW if self.dshow else 0))
        self.vid.set(cv2.CAP_PROP_FPS, self.fps)

        if not self.vid.isOpened():
            raise ValueError('Unable to open self.video source')
        print(f'Set camera capture properties')
        self.vid.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        self.vid.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)

        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

        self.vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self.vid.set(cv2.CAP_PROP_FOCUS, self.focus)
        self.vid.set(cv2.CAP_PROP_EXPOSURE, self.exposure)
        self.vid.set(cv2.CAP_PROP_CONTRAST, self.contrast)
        self.vid.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)

    def increase_exposure(self):
        self.exposure += 0.5
        print(f'exposure: {self.exposure}')
        self.vid.set(cv2.CAP_PROP_EXPOSURE, self.exposure)

    def decrease_exposure(self):
        self.exposure -= 0.5
        print(f'exposure: {self.exposure}')
        self.vid.set(cv2.CAP_PROP_EXPOSURE, self.exposure)

    def increase_brightness(self):
        self.brightness += 10
        print(f'brightness: {self.brightness}')
        self.vid.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)

    def decrease_brightness(self):
        self.brightness -= 10
        print(f'brightness: {self.brightness}')
        self.vid.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)

    def increase_contrast(self):
        self.contrast += 10
        print(f'contrast: {self.contrast}')
        self.vid.set(cv2.CAP_PROP_CONTRAST, self.contrast)

    def decrease_contrast(self):
        self.contrast -= 10
        print(f'contrast: {self.contrast}')
        self.vid.set(cv2.CAP_PROP_CONTRAST, self.contrast)

    def increase_focus(self):
        self.focus += 5
        print(f'focus: {self.focus}')
        self.vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self.vid.set(cv2.CAP_PROP_FOCUS, self.focus)

    def decrease_focus(self):
        self.focus -= 5
        print(f'focus: {self.focus}')
        self.vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self.vid.set(cv2.CAP_PROP_FOCUS, self.focus)
