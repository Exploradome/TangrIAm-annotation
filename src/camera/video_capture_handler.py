import cv2
import configparser
from .camera_handler import CameraHandler

class VideoCaptureHandler:
    def __init__(self, camera: CameraHandler, left_or_right: str, config: configparser.ConfigParser):
        self.camera = camera
        self.left_or_right = left_or_right

        self.camera_invert_colors = config.getboolean('INVERT_COLORS')
        self.camera_crop_top = int(config['CAMERA_CROP_TOP'])
        self.camera_crop_bottom = int(config['CAMERA_CROP_BOTTOM'])
        self.camera_crop_left = int(config['CAMERA_CROP_LEFT'])
        self.camera_crop_right = int(config['CAMERA_CROP_RIGHT'])
        self.playarea_top = int(config['PLAYAREA_TOP'])
        self.playarea_bottom = int(config['PLAYAREA_BOTTOM'])
        self.playarea_left = int(config['PLAYAREA_LEFT'])
        self.playarea_right = int(config['PLAYAREA_RIGHT'])

        self.vid = None
        self.camera.open_camera()
        self.compute_crop()

    def compute_crop(self):
        self.left_crop_dimensions = (
            int(self.camera_crop_top + self.playarea_left),
            int(self.camera.height - self.camera_crop_bottom - self.playarea_right),
            int(self.camera_crop_left + self.playarea_bottom),
            int(self.camera.width / 2 - self.playarea_top))
        self.right_crop_dimensions = (
            int(self.camera_crop_top + self.playarea_left),
            int(self.camera.height - self.camera_crop_bottom - self.playarea_right),
            int(self.camera.width / 2 + self.playarea_top),
            int(self.camera.width - self.playarea_bottom - self.camera_crop_right))

    def get_frame(self):
        if self.camera.vid and self.camera.vid.isOpened():
            _, frame = self.camera.vid.read()
            if frame is not None:
                if self.camera_invert_colors:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                if self.left_or_right == 'left':
                    left_frame = frame[self.left_crop_dimensions[0]:self.left_crop_dimensions[1],
                                       self.left_crop_dimensions[2]:self.left_crop_dimensions[3]]
                    return left_frame
                else:
                    right_frame = frame[self.right_crop_dimensions[0]:self.right_crop_dimensions[1],
                                        self.right_crop_dimensions[2]:self.right_crop_dimensions[3]]
                    return right_frame

    def __del__(self):
        if self.vid is not None and self.vid.isOpened():
            self.vid.release()
