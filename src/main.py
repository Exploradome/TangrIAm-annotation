import configparser

from annotation.display import Display

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('../config.ini', "utf8")

    Display('left', '../camera_data', '../training_data', model_path='../models', config=config)
