import cv2
import numpy as np
import math

class ImageProcessor:
    @staticmethod
    def process_image(image: np.ndarray, im_width: int, im_height: int, rotation, min_cropped_img_shape=50, max_cropped_img_shape=350):
        image_with_bounding_box = None
        segmented_and_cropped_image = None
        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        mean_threshold = 25
        segmented_img_bin = ImageProcessor.segment_image(gray)
        image_to_classify = segmented_img_bin
        segmented_img_rgb = cv2.cvtColor(segmented_img_bin, cv2.COLOR_GRAY2RGB)
        segmented_img_rgb_plus_box = segmented_img_rgb.copy()
        contours = ImageProcessor.find_contours(segmented_img_bin)
        contours, bboxes, reason = ImageProcessor.draw_bounding_boxes(contours, gray, segmented_img_rgb_plus_box, 1500, 300000)
        segmented_and_cropped_image = segmented_img_rgb_plus_box

        outer = ImageProcessor.get_outer_box(bboxes)
        # mean_distance = ImageProcessor.mean_between_bboxes(bboxes)
        # if mean_distance < mean_threshold and outer is not None:
        if outer is not None:
            x, y, w, h = outer
            image_with_bounding_box = image.copy()
            ImageProcessor.draw_rectangle(image_with_bounding_box, x, y, w, h)

            largest_side = w if w > h else h
            center_x = x + w // 2
            center_y = y + h // 2
            segmented_and_cropped_image = segmented_img_rgb_plus_box[
                                               max(0, center_y - largest_side // 2): min(center_y + largest_side // 2, segmented_img_rgb_plus_box.shape[1]),
                                               max(0, center_x - largest_side // 2): min(center_x + largest_side // 2, segmented_img_rgb_plus_box.shape[0])]
            if segmented_and_cropped_image.size == 0:
                segmented_and_cropped_image = segmented_img_rgb_plus_box

            image_to_classify = segmented_img_bin[y:y + h, x:x + w]
            
            if image_to_classify.shape[0] < min_cropped_img_shape or \
                image_to_classify.shape[1] < min_cropped_img_shape:
                reason = "too_small"
            if image_to_classify.shape[0] > max_cropped_img_shape or \
                image_to_classify.shape[1] > max_cropped_img_shape:
                reason = "too_big"
            # detect hand on edge
            if rotation == cv2.ROTATE_90_CLOCKWISE and x + w > segmented_img_bin.shape[1] - 5:
                reason = "hand"
            if rotation == cv2.ROTATE_90_COUNTERCLOCKWISE and x < 5:
                reason = "hand"
        else:
            # if mean_distance >= mean_threshold:
                # reason = "scattered" + str(mean_distance)
            image_with_bounding_box = image.copy()
        image_to_classify = ImageProcessor.prepare_image_for_classification(image_to_classify, im_width, im_height, rotation)
        return image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason

    @staticmethod
    def prepare_image_for_classification(image_to_classify: np.ndarray, im_width: int, im_height: int, rotation):
        image_to_classify = cv2.resize(image_to_classify, (im_height, im_width))
        if rotation is not None:
            image_to_classify = cv2.rotate(image_to_classify, rotation)
        image_to_classify = ImageProcessor.segment_image(image_to_classify, cv2.THRESH_BINARY)
        image_to_classify = cv2.cvtColor(image_to_classify, cv2.COLOR_GRAY2RGB)
        return image_to_classify
    
    @staticmethod
    def segment_image(image: np.ndarray, bin = cv2.THRESH_BINARY_INV) -> np.ndarray:
        # blurred = cv2.GaussianBlur(image, (5, 5), 0)
        # segmented_img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
        _, segmented_img = cv2.threshold(image, 128, 255, bin)
        return segmented_img

    @staticmethod
    def draw_bounding_boxes(contours, gray, rgb, min_rectangle_area, max_rectangle_area):
        selected_cnt = []
        bboxes = []
        reason = 'ok'
        if contours is None or len(contours) == 0:
            reason = "no_contours"
            return selected_cnt, bboxes, reason
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if min_rectangle_area < w * h < max_rectangle_area:
                bboxes.append((x, y, w, h))
                selected_cnt.append(cnt)
                if gray is not None:
                    ImageProcessor.draw_rectangle(gray, x, y, w, h)
                if rgb is not None:
                    ImageProcessor.draw_rectangle(rgb, x, y, w, h)
        return selected_cnt, bboxes, reason

    @staticmethod
    def get_outer_box(bboxes: list):
        merged = None
        for box in bboxes:
            if merged is None:
                merged = box
            else:
                merged = ImageProcessor.union(merged, box)
        return merged

    @staticmethod
    def union(a, b):
        x = min(a[0], b[0])
        y = min(a[1], b[1])
        w = max(a[0] + a[2], b[0] + b[2]) - x
        h = max(a[1] + a[3], b[1] + b[3]) - y
        return x, y, w, h

    @staticmethod
    def draw_rectangle(image, x, y, w, h, color = (0, 255, 0)):
        image = cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
        return image

    @staticmethod
    def find_contours(segmented_img: np.ndarray) -> list:
        contours, _ = cv2.findContours(segmented_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return contours

    @staticmethod
    def mean_between_bboxes(bboxes: list):
        if len(bboxes) <= 1:
            return 0
        min_distances = [0]
        for i in range(len(bboxes)):
            b1 = bboxes[i]
            distances = []
            for j in range(0, len(bboxes)):
                if i == j:
                    continue
                b2 = bboxes[j]
                dist = ImageProcessor.distance_between_two_bbox(b1, b2)
                if dist > 0:
                    distances.append(dist)
                else:
                    distances = []
                    break
            #print('distances', distances)
            if len(distances) > 0:
                min_distances.append(min(distances))
        #print('min', min_distances)
        if min_distances == []:
            return 0
        return np.mean(min_distances)

    @staticmethod
    def distance_between_two_bbox(a, b):
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        if a[0] <= b[0] <= a[0] + a[2] or b[0] <= a[0] <= b[0] + b[2]:
            w = 0
        else:
            w = min(a[0]+a[2], b[0]+b[2]) - x
        if a[1] <= b[1] <= a[1] + a[3] or b[1] <= a[1] <= b[1] + b[3]:
            h = 0
        else:
            h = min(a[1]+a[3], b[1]+b[3]) - y
        return 0 if w > 0 and h > 0 else math.hypot(w,h)

    @staticmethod
    # Very slow
    def min_distance_between_contours_edges(contour1, contour2):
        min_distance = float('inf')
        for edge1 in contour1:
            for edge2 in contour2:
                distance = math.dist(edge1[0], edge2[0])
                if distance < min_distance:
                    min_distance = distance
        return min_distance
    
    @staticmethod
    def resize_images(img, image_width, image_height):
        if img is not None:
            img = cv2.resize(img, (image_height, image_width))
        return img
