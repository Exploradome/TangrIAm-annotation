import numpy as np
from cv2 import resize

from tensorflow import keras
from tensorflow.keras import models, layers, Model, losses
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.preprocessing.image import ImageDataGenerator

def get_labelled_data(data_path: str, im_height: int, im_width: int, data_augmentation: bool) -> tuple:
    if data_augmentation:
        datagen = ImageDataGenerator(
            validation_split=0.2,
            rotation_range=20,
            brightness_range=(0.0, 0.4),
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True)

        train_generator = datagen.flow_from_directory(
            data_path,
            subset='training',
            target_size=(im_height, im_width),
            batch_size=32)
        validation_generator = datagen.flow_from_directory(
            data_path,
            subset='validation',
            target_size=(im_height, im_width),
            batch_size=32)

        return train_generator, validation_generator

    else:
        train_ds = image_dataset_from_directory(
            data_path,
            validation_split=0.2,
            subset='training',
            label_mode='categorical',
            seed=42,
            image_size=(im_height, im_width),
            batch_size=32)

        test_ds = image_dataset_from_directory(
            data_path,
            validation_split=0.2,
            subset='validation',
            label_mode='categorical',
            seed=42,
            image_size=(im_height, im_width),
            batch_size=32)

        return train_ds, test_ds

def build_on_top_of_pretrained_model(pretrained_model, n_classes: int, im_height: int, im_width: int, channels: int, units = 128):
    model_input = layers.Input(shape=(im_height, im_width, channels))

    if channels == 1:
        base_model = layers.Flatten()(model_input)
        base_model = layers.RepeatVector(3)(base_model)
        base_model = layers.Reshape((im_height, im_width, 3))(base_model)
        base_model = pretrained_model(input_shape=(im_height, im_width, 3),
                                      weights='imagenet',
                                      include_top=False)(base_model)
        base_model.trainable = False

        head_model = layers.GlobalAveragePooling2D()(base_model)
        head_model = layers.Dense(units, activation='relu')(head_model)

        if n_classes == 2:
            head_model = layers.Dense(n_classes, activation='sigmoid')(head_model)
        else:
            head_model = layers.Dense(n_classes, activation='softmax')(head_model)

        model = Model(inputs=model_input, outputs=head_model)

    else:
        base_model = pretrained_model(input_shape=(im_height, im_width, 3),
                                      weights='imagenet',
                                      include_top=False)
        base_model.trainable = False

        head_model = layers.GlobalAveragePooling2D()(base_model.output)

        if n_classes == 2:
            head_model = layers.Dense(n_classes, activation='sigmoid')(head_model)
        else:
            head_model = layers.Dense(n_classes, activation='softmax')(head_model)

        model = Model(inputs=base_model.input, outputs=head_model)

    return model


def train_model(model: Model, train_data, test_data, model_path: str, n_classes: int, optimizer, epochs: int):
    if n_classes == 2:
        loss = losses.BinaryCrossentropy(from_logits=True)
    else:
        loss = losses.CategoricalCrossentropy(from_logits=True)

    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

    early_stop = EarlyStopping(monitor='val_loss', patience=5, mode='min', verbose=1)
    callbacks = [early_stop, ModelCheckpoint(model_path,
                                             monitor='val_loss',
                                             # monitor='accuracy',
                                             save_best_only=True,
                                             mode='min',
                                             # mode='max',
                                             verbose=1)]

    history = model.fit(train_data, validation_data=test_data,
                        batch_size=32, epochs=epochs, callbacks=callbacks)
    return model, history










