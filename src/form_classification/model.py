import numpy as np
import cv2
from .image_processor import ImageProcessor
from .gradcam import GradCam
from tensorflow import keras  

class Model:
    def __init__(self, model_path: str, never_predicts: bool, number_of_top_predictions: int, im_width: int, im_height: int):
        self.cache_top_n_predictions = {cv2.ROTATE_90_CLOCKWISE: None, cv2.ROTATE_90_COUNTERCLOCKWISE: None}
        self.cache_explanation = {cv2.ROTATE_90_CLOCKWISE: None, cv2.ROTATE_90_COUNTERCLOCKWISE: None}
        self.cache_reason = {cv2.ROTATE_90_CLOCKWISE: 'ok', cv2.ROTATE_90_COUNTERCLOCKWISE: 'ok'}
        self.model_path = model_path
        self.number_of_top_predictions = number_of_top_predictions
        self.never_predicts = never_predicts
        self.im_height = im_height
        self.im_width = im_width
        self.model:keras.Model = None
        self.gradcam:GradCam = None
        self.always_show_explanation = True

    def get_trained_model(self):
        self.model = keras.models.load_model(self.model_path)
        if self.model is None:
            print("Can't find model", self.model_path)
        self.gradcam = GradCam(self.model)
        return self.model    

    def ai_warm_up(self):
        if not self.never_predicts:
            print("AI warm up")
            self.get_top_n_predictions(np.zeros((self.im_height, self.im_width, 3), np.uint8), 1)

    def get_top_n_predictions(self, image: np.ndarray, n: int = None, with_fix = False):
        if n is None:
            n = self.number_of_top_predictions
            
        top_n_predictions = {}
        image = cv2.resize(image, (self.im_width, self.im_height))
        predictions = self.model.predict(image.reshape(1, image.shape[0], image.shape[1], 3))[0]
        top_n_predictions, reason = self.get_top_n_results(predictions, n, with_fix)
        if top_n_predictions is None:
            return None, predictions, reason
        else:
            return top_n_predictions, predictions, reason

    def get_top_n_results(self, predictions: np.ndarray, n: int = None, with_fix = False):
        if n is None:
            n = self.number_of_top_predictions
        top_n_predictions = {}
        # retrieve one more prediction to skip 'none' if it is the result
        for index in predictions.argsort()[::-1][:n + 1]:
            top_n_predictions[index] = predictions[index]
        if (0 in top_n_predictions):
            if list(top_n_predictions.keys()).index(0) == 0:
                return None, '_none_'
            else:
                top_n_predictions.pop(0)
        else:
            # remove last superfluous item
            top_n_predictions.popitem()
        # get first element from top_n_predictions
        
        ref_score = top_n_predictions[list(top_n_predictions.keys())[0]]
        revised_top_n_predictions = {}
        for i, label in enumerate(top_n_predictions):
            score = top_n_predictions[label]
            # MAKE SMALL PREDICTIONS VISIBLE:
            if with_fix and score < 0.30:
                while score < ref_score - 0.3:
                    score *= 10
                while score >= ref_score:
                    score /= 5
            # shift index because labels start at 0 (without none)
            revised_top_n_predictions[label - 1] = score
        # sort by score
        sorted(revised_top_n_predictions.items(), key=lambda x: x[1], reverse=True)
        return dict(sorted(revised_top_n_predictions.items(), key=lambda x: x[1], reverse=True)), 'ok'

    def process_frame_and_predict(self, frame: np.ndarray, rotation, must_predict: bool):
        image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason = \
            ImageProcessor.process_image(frame, self.im_width, self.im_height, rotation) 
        explanation = None
        if reason == 'ok':
            if must_predict and self.model is not None:
                top_n_predictions, predictions, reason = self.get_top_n_predictions(image_to_classify, self.number_of_top_predictions, True)    
                if top_n_predictions is not None:
                    explanation = self.explain_prediction(list(top_n_predictions.keys())[0], image_to_classify)
                    scores = list(top_n_predictions.items())
                    # cache predictions between predict
                    self.cache_top_n_predictions[rotation] = scores
                    self.cache_explanation[rotation] = explanation
                    self.cache_reason[rotation] = reason # 'ok'
                else: # none label
                    if self.always_show_explanation:
                        explanation = self.explain_prediction(0, image_to_classify)
                    self.cache_explanation[rotation] = explanation
                    self.cache_top_n_predictions[rotation] = None
                    self.cache_reason[rotation] = reason # '_none_'
            else: # no prediction needed
                if self.always_show_explanation:
                    explanation = self.explain_prediction(0, image_to_classify)
        else: # too big or too small
            if self.always_show_explanation:
                explanation = self.explain_prediction(0, image_to_classify)
            self.cache_top_n_predictions[rotation] = None
            self.cache_explanation[rotation] = explanation
            self.cache_reason[rotation] = reason
                
        return image_with_bounding_box, segmented_and_cropped_image, \
                image_to_classify, self.cache_explanation[rotation], self.cache_top_n_predictions[rotation], self.cache_reason[rotation]

    def explain_prediction(self, top_prediction, image: np.ndarray) -> np.ndarray:
        if self.gradcam is None:
            return None
        heatmap = self.gradcam.compute_heatmap(image.reshape(1, image.shape[0], image.shape[1], 3),
                                                top_prediction) 

        overlay = self.gradcam.overlay_heatmap(heatmap, image, alpha=0.2)
        overlay = cv2.cvtColor(overlay, cv2.COLOR_BGR2RGB)
        return overlay