import matplotlib.pyplot as plt

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2

from form_classification.model_trainer import build_on_top_of_pretrained_model, get_labelled_data, train_model


def plot_history(training_history):
    acc = training_history.history['accuracy']
    val_acc = training_history.history['val_accuracy']

    loss = training_history.history['loss']
    val_loss = training_history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')
    plt.show()


if __name__ == '__main__':
    train_ds, test_ds = get_labelled_data('../training_data', 224, 224, data_augmentation=True)

    n_classes = 3

    model_path = f'../models/model_{n_classes}classes'

    model = build_on_top_of_pretrained_model(MobileNetV2, n_classes, 224, 224, 3)
    opt = Adam(lr=1e-3, decay=1e-5)

    trained_model, history = train_model(model, train_ds, test_ds, model_path, n_classes, opt, epochs=30)
