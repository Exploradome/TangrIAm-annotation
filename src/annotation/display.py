import codecs
import tkinter

import cv2
from PIL import ImageTk, Image
from datetime import datetime
import os
import tensorflow as tf

from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.optimizers import Adam

from camera.camera_handler import CameraHandler
from camera.video_capture_handler import VideoCaptureHandler
from form_classification import model_trainer
from form_classification.image_processor import ImageProcessor
from utils.utils import create_directory_if_does_not_exist


class Display:
    def __init__(self, left_or_right: str, camera_data_path: str, cropped_data_path: str, model_path: str, config):
        self.config = config
        self.window = tkinter.Tk()
        self.left_or_right = left_or_right
        self.camera_data_path = camera_data_path
        self.cropped_data_path = cropped_data_path
        self.model_path = model_path
        self.camera_config = config['CAMERA']

        self.camera = CameraHandler(self.camera_config)
        self.video_capture = VideoCaptureHandler(self.camera, self.left_or_right, self.camera_config)

        self.window_width = 1280

        self.top_canvas = tkinter.Canvas(self.window, width=self.window_width, height=40, bg='white')
        self.top_canvas.pack(side=tkinter.TOP)

        self.canvas = tkinter.Canvas(self.window, width=self.window_width, height=800, bg='white')
        self.canvas.pack(side=tkinter.BOTTOM)

        self.title = self.top_canvas.create_text(10, 10, text='Label:', font=('Arial', 14), fill='black',
                                                 anchor=tkinter.NW)
        self.label = tkinter.Entry(self.top_canvas)
        self.label.place(anchor=tkinter.NW, height=30, width=100, x=80, y=10)

        self.btn_film = tkinter.Button(self.top_canvas, text='Start filming', command=self.start_filming)
        self.btn_film.place(anchor=tkinter.NW, height=30, width=200, x=200, y=10)

        self.btn_stop = tkinter.Button(self.top_canvas, text='Stop', command=self.stop)
        self.btn_stop.place(anchor=tkinter.NW, height=30, width=60, x=440, y=10)

        self.btn_take_picture = tkinter.Button(self.top_canvas, text='Take picture', command=self.take_picture)
        self.btn_take_picture.place(anchor=tkinter.NW, height=30, width=200, x=540, y=10)

        self.btn_train = tkinter.Button(self.top_canvas, text='Train model', command=self.train_model)
        self.btn_train.place(anchor=tkinter.NW, height=30, width=200, x=780, y=10)

        self.model_name = tkinter.Entry(self.top_canvas)
        self.model_name.place(anchor=tkinter.NW, height=30, width=100, x=1000, y=10)

        self.tk_preview_live = self.canvas.create_image(0, 0, image=None, anchor=tkinter.NW)
        self.tk_preview_segmented = self.canvas.create_image(400, 0, image=None, anchor=tkinter.NW)
        self.tk_preview_with_bounding_box = self.canvas.create_image(650, 0, image=None, anchor=tkinter.NW)
        self.tk_preview_to_classify = self.canvas.create_image(400, 250, image=None, anchor=tkinter.NW)

        self.filming = False

        self.delay = 15
        self.preview_live = None
        self.preview_segmented = None
        self.preview_with_bounding_box = None
        self.preview_to_classify = None
        self.raw_frame = None
        self.image_to_classify = None

        self.keyboard_bindings = True
        self.setKeyboardBindings()
        self.update()

        self.window.mainloop()

    def destroy(self):
        self.camera.destroy()
        self.window.destroy()
        self.window.quit()        

    def setKeyboardBindings(self):
        self.label.bind('<FocusIn>', lambda e: self.disableKeyboardBindings())
        self.label.bind('<FocusOut>', lambda e: self.enableKeyboardBindings())
        self.window.bind('q', lambda e: self.window.quit() if self.keyboard_bindings else None)
        self.window.bind('<Escape>', lambda e: self.destroy())
        self.window.bind('<Return>', lambda e: self.start_filming())
        self.window.bind('<space>', lambda e: self.take_picture())
        self.window.bind('s', lambda event: self.save_config() if self.keyboard_bindings else None)
        self.window.bind('E', lambda event: self.camera.increase_exposure() if self.keyboard_bindings else None)
        self.window.bind('e', lambda event: self.camera.decrease_exposure() if self.keyboard_bindings else None)
        self.window.bind('B', lambda event: self.camera.increase_brightness() if self.keyboard_bindings else None)
        self.window.bind('b', lambda event: self.camera.decrease_brightness() if self.keyboard_bindings else None)
        self.window.bind('C', lambda event: self.camera.increase_contrast() if self.keyboard_bindings else None)
        self.window.bind('c', lambda event: self.camera.decrease_contrast() if self.keyboard_bindings else None)
        self.window.bind('F', lambda event: self.camera.increase_focus() if self.keyboard_bindings else None)
        self.window.bind('f', lambda event: self.camera.decrease_focus() if self.keyboard_bindings else None)
        self.window.bind('0', lambda event: self.camera.set_camera_id(0) if self.keyboard_bindings else None)
        self.window.bind('1', lambda event: self.camera.set_camera_id(1) if self.keyboard_bindings else None)
        self.window.bind('2', lambda event: self.camera.set_camera_id(2) if self.keyboard_bindings else None)


    def save_config(self):
        self.config['CAMERA']['ID'] = str(self.camera.camera_id)
        self.config['CAMERA']['EXPOSURE'] = str(self.camera.exposure)
        self.config['CAMERA']['BRIGHTNESS'] = str(self.camera.brightness)
        self.config['CAMERA']['CONTRAST'] = str(self.camera.contrast)
        self.config['CAMERA']['FOCUS'] = str(self.camera.focus)
        with codecs.open('../config.ini', 'w', 'utf-8') as configfile:
            self.config.write(configfile)

    def enableKeyboardBindings(self):
        self.keyboard_bindings = True

    def disableKeyboardBindings(self):
        self.keyboard_bindings = False

    def update(self):
        frame = self.video_capture.get_frame()
        if frame is not None:
            self.update_frame(frame)

        self.window.after(self.delay, self.update)

    def start_filming(self):
        self.filming = True

    def stop(self):
        self.filming = False

    def take_picture(self):
        if self.image_to_classify is not None:
            camera_data_directory = os.path.join(self.camera_data_path, self.label.get())
            cropped_directory = os.path.join(self.cropped_data_path, self.label.get())
            create_directory_if_does_not_exist(camera_data_directory)
            create_directory_if_does_not_exist(cropped_directory)
            frame_name = f'frame-{datetime.now().strftime("%Y%m%d-%H%M%S.%f")}'
            print('Saving', frame_name)
            cv2.imwrite(os.path.join(camera_data_directory, f'{frame_name}.png'), self.raw_frame)
            cv2.imwrite(os.path.join(cropped_directory, f'{frame_name}_cropped.png'), self.image_to_classify)
        else:
            print('No frame available')

    def update_frame(self, frame):
        image_with_bounding_box, segmented_and_cropped_image, image_to_classify, reason = \
            ImageProcessor.process_image(frame, 224, 224, cv2.ROTATE_90_COUNTERCLOCKWISE)
        
        if image_to_classify is not None:
            self.btn_film.config(state="normal")
            self.btn_take_picture.config(state="normal")
            self.raw_frame = frame
            self.image_to_classify = image_to_classify
            if self.filming:
                self.take_picture()
        else:
            self.image_to_classify = None
            self.btn_film.config(state="disabled")
            self.btn_take_picture.config(state="disabled")

        self.preview_live = ImageTk.PhotoImage(image=Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)))
        self.canvas.itemconfigure(self.tk_preview_live, image=self.preview_live)

        if segmented_and_cropped_image is not None:
            self.preview_segmented = ImageTk.PhotoImage(image=Image.fromarray(ImageProcessor.resize_images(segmented_and_cropped_image, 224, 224)))
            self.canvas.itemconfigure(self.tk_preview_segmented, image=self.preview_segmented)

        if image_with_bounding_box is not None:
            self.preview_with_bounding_box = ImageTk.PhotoImage(image=Image.fromarray(
                ImageProcessor.resize_images(cv2.cvtColor(image_with_bounding_box, cv2.COLOR_BGR2RGB), 224, 224)))
            self.canvas.itemconfigure(self.tk_preview_with_bounding_box, image=self.preview_with_bounding_box)

        if image_to_classify is not None:
            self.preview_to_classify = ImageTk.PhotoImage(image=Image.fromarray(image_to_classify))
            self.canvas.itemconfigure(self.tk_preview_to_classify, image=self.preview_to_classify)

    def train_model(self):
        train_ds, test_ds = model_trainer.get_labelled_data(self.cropped_data_path, 224, 224, data_augmentation=True)
        n_classes = len(test_ds.class_indices)
        print('Labels:', n_classes, test_ds.class_indices.keys())
        
        units = 128
        epoch = 15
        model_path = os.path.join(self.model_path, f'model_{n_classes}classes_{datetime.now().strftime("%Y%m%d-%H%M%S")}' + \
                                  self.model_name.get() + "_" + str(units) + "_" + str(epoch))

        model: tf.keras.Model = model_trainer.build_on_top_of_pretrained_model(MobileNetV2, n_classes, 224, 224, 3, units)
        opt = tf.keras.optimizers.Adam(lr=1e-3, decay=1e-5)

        model_trainer.train_model(model, train_ds, test_ds, model_path, n_classes, opt, epoch)

        self.update()
